#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/socket.h>
#include <netdb.h>
#include <poll.h>

#define PORT_DEFAULT "5690"
#define PORT_LEN 4

#define MSG_LEN 128

#define BACKLOG 5
#define CALL_TIMEOUT_MS 60000

enum {MODE_CALL, MODE_ACCEPT} mode = MODE_CALL;
char port[PORT_LEN + 1] = PORT_DEFAULT;

char is_all_digits(const char *str) {
	while (*str != 0) {
		if (*str < '0' || *str > '9') {
			return 0;
		}
		++str;
	}
	return 1;
}

void get_custom_port(const char **argv, const int idx) {
	// Check if the value in `argv[idx]` is a valid port number and
	// and then copy it into `port`.
	if (strlen(argv[idx]) > PORT_LEN || !is_all_digits(argv[idx])) {
		fputs("Wrong port format\n", stderr);
		exit(EXIT_FAILURE);
	}
	strcpy(port, argv[idx]);
}

#define CREATE_SOCKET_FAILURE -1
int create_socket(const char *ip) {
	// Create a socket and either `bind` or `connect` it to `port` depending on `mode`.
	int rc = 0;
	struct addrinfo *ai, hints;
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;
	rc = getaddrinfo(ip, port, &hints, &ai);
	if (rc != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rc));
		return CREATE_SOCKET_FAILURE;
	}
	struct addrinfo *addr;
	for (addr = ai; addr != NULL; addr = addr->ai_next) {
		if (addr->ai_family == AF_INET)
			break;
	}
	if (addr == NULL) {
		fputs("getaddrinfo: No suitable address info found\n", stderr);
		freeaddrinfo(ai);
		return -1;
	}
	int fd = socket(addr->ai_family, addr->ai_socktype, addr->ai_protocol);
	if (fd == -1) {
		perror("socket");
		freeaddrinfo(ai);
		return CREATE_SOCKET_FAILURE;
	}
	if (mode == MODE_ACCEPT) {
		rc = bind(fd, addr->ai_addr, addr->ai_addrlen);
		if (rc == -1) {
			perror("bind");
			freeaddrinfo(ai);
			return CREATE_SOCKET_FAILURE;
		}
	} else if (mode == MODE_CALL) {
		rc = connect(fd, addr->ai_addr, addr->ai_addrlen);
		if (rc == -1) {
			perror("connect");
			freeaddrinfo(ai);
			return CREATE_SOCKET_FAILURE;
		}
	}
	freeaddrinfo(ai);
	return fd;
}

int call_routine(int fd) {
	// Start exchanging stdin messages with `fd`.
	struct pollfd pfds[2];
	char msg[MSG_LEN];
	int rc = 0;
	puts("call accepted");
	pfds[0] = (struct pollfd){.fd = STDIN_FILENO, .events = POLLIN};
	pfds[1] = (struct pollfd){.fd = fd, .events = POLLIN};
	while ((rc = poll(pfds, 2, CALL_TIMEOUT_MS))) {
		char valid_poll = 0;
		//printf("[d]: poll rc: %d\n", rc);
		if (pfds[0].revents & POLLIN) {
			//puts("[d]: valid_poll on stdin");
			valid_poll = 1;
			fgets(msg, MSG_LEN, stdin); // XXX no error and EOF handling
			if (ferror(stdin)) {
				fputs("fgets: Error reading from stdin\n", stderr);
				break;
			}
			if (feof(stdin)) {
				puts("");
				break;
			}
			msg[strlen(msg) - 1] = 0;
			//printf("Got stdin: %s\n", msg);
			rc = send(fd, msg, strlen(msg), 0); // XXX no error and partial delivery handling
		}
		if (pfds[1].revents & POLLIN) {
			//puts("[d]: valid_poll on fd");
			valid_poll = 1;
			rc = recv(fd, msg, MSG_LEN - 1, 0);
			if (rc == 0) {
				fputs("recv: Connection closed\n", stderr);
				break;
			} else if (rc == -1) {
				perror("recv");
				break;
			}
			msg[rc] = 0;
			printf("[r]: %s\n", msg);
		}
		if (!valid_poll) {
			// this probably means connection closed on the other end
			break;
		}
	}
	puts("call finished");
	return EXIT_SUCCESS;
}

int accept_routine(int sock) {
	// Start listening on `sock` accepting all incoming calls and passing them to `call_routine`.
	// TODO: handle stdin while there is no `call_routine` in progress
	int rc = 0;
	rc = listen(sock, BACKLOG);
	if (rc == -1) {
		perror("listen");
		return EXIT_FAILURE;
	}
	struct sockaddr_storage caller_addr;
	while (1) {
		socklen_t caller_addr_sz = sizeof caller_addr;
		int fd = accept(sock, (struct sockaddr *)&caller_addr, &caller_addr_sz);
		if (fd == -1) {
			perror("accept");
			continue;
		}
		call_routine(fd);
		close(fd);
		clearerr(stdin); // so that we are able to fgets() again after ^D
	}
	return EXIT_SUCCESS;
}

int main(const int argc, const char **argv) {
	switch (argc) {
		case 2:
			get_custom_port(argv, 1);
			__attribute__ ((fallthrough));
		case 1:
			mode = MODE_ACCEPT;
			break;
		case 4:
			get_custom_port(argv, 3);
			__attribute__ ((fallthrough));
		case 3:
			mode = MODE_CALL;
			break;
		default:
			fputs("Wrong number of arguments\n", stderr);
			puts("Usage: chat [port]                \taccept calls on `port`");
			puts("Usage: chat call <address> [port] \tcall `address:port`");
			exit(EXIT_FAILURE);
	}
	const int sock = create_socket(mode == MODE_CALL? argv[2]: NULL);
	if (sock == CREATE_SOCKET_FAILURE) {
		return EXIT_FAILURE;
	}
	const int rc = mode == MODE_ACCEPT? accept_routine(sock): call_routine(sock);
	close(sock);
	return rc;
}

