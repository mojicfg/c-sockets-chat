C=gcc
CFLAGS=-Wall -Wextra -Werror -Wpedantic -std=c99 -D_GNU_SOURCE

chat: chat.c
	$C $(CFLAGS) $< -o$@
